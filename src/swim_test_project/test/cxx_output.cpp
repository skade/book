// top=cxx::sample

#define TOP sample

#include <verilator_util.hpp>

TEST_CASE(it_works, {
    // ANCHOR: input_assignment
    s.i->a = "Some(5)";
    s.i->b = "10";
    s.i->c = "5";
    // ANCHOR_END: input_assignment

    ctx->timeInc(1);
    dut->eval();

    ctx->timeInc(1);
    dut->eval();

    // ANCHOR: output_check
    ASSERT_EQ(s.o, "SampleOutput$(a: Some(5), sub: SubStruct$(b: 10, c: 5))");
    ASSERT_EQ(s.o->a, "Some(5)");
    ASSERT_EQ(s.o->sub, "SubStruct$(b: 10, c: 5)");
    ASSERT_EQ(s.o->sub->b, "10");
    ASSERT_EQ(s.o->sub->c, "5");
    // ANCHOR_END: output_check
    return 0;
})

MAIN
