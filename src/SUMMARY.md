# Summary


- [Introduction](./introduction.md)
- [Installation](./installation.md)
- [Simulation and Testing](./simulation.md)

# Example projects

- [Sample project: ws2128 RGB LED driver](./ws2812-example/index.md)
    - [Creating a Project](./ws2812-example/project.md)
    - [Led Protocol Overview](./ws2812-example/led_protocol_overview.md)
    - [Driver Interface](./ws2812-example/driver_interface.md)
    - [State Machine](./ws2812-example/state_machine.md)
    - [Testing the State Machine](./ws2812-example/testing_the_state_machine.md)
    - [Output generation](./ws2812-example/output_generation.md)
    - [Testing in hardware](./ws2812-example/testing_in_hardware.md)

# Language & Tool References

- [Language Reference](./language_reference/index.md)
    - [Items](./language_reference/items.md)
        - [Units](./language_reference/items/units.md)
        - [Type Delcarations](./language_reference/items/type_declarations.md)
    - [Statements](./language_reference/statements.md)
    - [Expressions](./language_reference/expressions.md)
        - [Instantiation](./language_reference/expressions/instantiation.md)
        - [Indexing](./language_reference/expressions/indexing.md)
        - [Stage References](./language_reference/expressions/stage_reference.md)
    - [Patterns](./language_reference/pattern.md)
        - [Refutability](./language_reference/pattern/refutability.md)
    - [Type system](./language_reference/type_system/index.md)
        - [Inverted Ports](./language_reference/type_system/inverted_ports.md)
    - [Dynamic Pipelines](./language_reference/dynamic_pipelines.md)
    - [Binding](./binding.md)
    - [Constructs by syntax](./language_reference/constructs_by_syntax.md)

- [Swim Project Configuration](./swim_project_configuration/config__Config.md)
- [Swim Plugin Configuration](./swim_project_configuration/plugin__config__PluginConfig.md)

---

- [Compiler Internals](./internal/index.md)
    - [Naming](./internal/naming.md)
    - [Type Representation](./internal/type_representations.md)
