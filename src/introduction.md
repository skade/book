# Spade

![Spade Logo](images/spadefish-white-outline-200.png)

[Spade](https://spade-lang.org) is a [Rust](https://rust-lang.org)-inspired
[hardware description
language](https://en.wikipedia.org/wiki/Hardware_description_language).

For a brief guided introduction to Spade, you can read the chapter on
[implementing a ws2128 RGB LED driver](./ws2812-example/index.md)

If you are more interested in a reference of all constructs in the language, see the [language reference](./language_reference/index.md)

If you have any questions or would like to discuss Spade with others, feel free to join the [Discord community](https://discord.gg/YtXbeamxEX).

> Spade is a work in progress language and so is this documentation.
> Writing documentation is hard since it is hard to know what details are
> obvious and what things need more explanation. To make the documentation better, your feedback is
> invaluable so if you notice anything that
> is unclear or needs more explanation, please, reach out either via a [gitlab
> issue](https://gitlab.com/spade-lang/book/-/issues/new) <!-- TODO link to issue
> creation --> or on [Discord](https://discord.gg/YtXbeamxEX)
> <!-- TODO: Not everyone likes discord, I'd like to set up a matrix mirror -->

- [The Spade website](https://spade-lang.org)
- [Discord community](https://discord.gg/YtXbeamxEX)
- [This book's repository](https://gitlab.com/spade-lang/book.git)
- [Spade's repository](https://gitlab.com/spade-lang/spade.git)

## Chapters

- [Sample project: ws2128 RGB LED driver](./ws2812-example/index.md)
- [Language Reference](./language_reference/index.md)
- [Compiler Internals](./internal/index.md)
