# Ws2812b Example


![](result.png)

This chapter will guide you through how to build a spade library for the
ws2812b RGB led and should serve as a practical example for "real world" Spade
usage.

This assumes a bit of familiarity with basic spade concepts, and is written
primarily with software people in mind, as such more weight will be put on the
FPGA specifics than on spade syntax and concepts.

The chapter starts off with a discussion on how to create a spade project and
how that project is laid out. After that, we will discuss the interfaces we
want to and need to use, i.e. how to talk to the LEDs, and how to make the
driver interface nice to use for other spade code. Finally, we'll go over the
implementation of the actual driver.
