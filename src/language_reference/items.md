# Items

Anything that appears at the top level of a Spade file is an item. This includes
[units](units.md), [types](types.md) and [(sub)modules](modules.md) etc..

As a user, you will rarely encounter the term `Item`, though it might appear in
parser errors if you write something unexpected at the top level of a file.
